provider "aws" {
    region = "us-west-1"
}

data "template_file" "user_data" {
  template = file("add-user-data.yml")
}

resource "aws_instance" "tf-ansible-example" {
    
    # CentOS Stream 8 AMI
    ami           = "ami-0f377b303df4963ab"
    instance_type = "t2.micro"
    subnet_id                   = "subnet-089debc6888a20987"
    associate_public_ip_address = true
    user_data                   = data.template_file.user_data.rendered

    key_name                    = "aws-west-ml"

  tags = {
    Name = "tf-ansible-example-centos-tomcat"
  }  
    provisioner "remote-exec" {
    inline = [
      "curl -d host_config_key=f742ef0d-4a09-4231-af6e-d4935f256f54 https://tower.ansibletowerlab.com:443/api/v2/job_templates/91/callback/ -k -v"
    ]

    connection {
      host        = "${self.public_ip}"
      type        = "ssh"
      user        = "ansible"
      private_key = "${file("tf-ansible")}"
    }
  }

}
